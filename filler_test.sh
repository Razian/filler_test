#!/bin/sh

echo "=== filler_test by tchivert ==="
read -p "Enter your username: " uname
read -p "Enter the full path of your project : " path
read -p "Do you want to see the details ? (y/N) " silent
read -p "Press enter to start..."
if [ "$silent" == "y" ]
then
	make -C $path re
	cp "$path/$uname.filler" ./
	./games.sh $uname | tee $uname.results
else
	make -C $path re > /dev/null
	cp "$path/$uname.filler" ./
	./games.sh $uname | tee $uname.results | ./visualizer.sh $uname
fi
rm $uname.filler
rm filler.trace
