#!/bin/sh

i=0
j=0
while read input
do
	if [[ $input == *"won"* ]]
	then
		((i++))
	fi
	if [ "$input" = "./$1.filler won" ]
	then
		((j++))
	fi
	printf "[Running] Played: $i/108 | Won: $j/108\r"
done
printf "\n"
if [ $j > 54 ]
then
	printf "[Finished] Well done, you won $j/108 tests !\n"
else
	printf "[Finished] You only won $j/108 tests, that's not enough. Keep up !\n"
fi
